<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BPL API</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.1.js"></script>
    <!-- <script src="{{asset('build/assets/product.300b3445.js')}}"></script> -->
    <!-- @vite('resources/js/product.js') -->
    
    
</head>
<body>
    <div>
        @yield('content')
    </div>
</body>

</html>
