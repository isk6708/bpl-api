@extends('layouts.app')
@section('title','Product')
@section('content')
<h1>Selamat Datang Javascript Real World</h1>

<div id='borang' style="display:none;">
    <h3>Borang Product</h3>
     <input type="hidden" name="product_id" id="product_id">
    Nama <input type="text" name="product_name" id="product_name">
    Kategori 
    
    <select name="productcat" id="productcat">
    <option value="">-- Sila Pilih --</option>
    </select>

    <br>
    <button onclick="simpan()" class="btn btn-info">Simpan</button>
    <button onclick="kembali()" class="btn btn-warning">Kembali</button>

</div>
<button onclick="tambah()" id="tambahbtn" class="btn btn-success">Tambah</button>
Nama Produk <input type="text" id="product_name_search">
<br>
Kategori Produk 
<select name="product_cat_search" id="product_cat_search">
<option value="">-- Sila Pilih --</option>
</select>
<!-- <input type="text" id="product_cat_search"> -->
<input type="button" onclick="senaraiProduk()" class="btn btn-primary" value="Teruskan">
<div id='senarai'>

<h3>Senarai Product</h3>
<!-- Senarai Produk akan diletak di sini -->
</div>
<script>
    

    function simpan(){
    if (confirm('Anda pasti?')){
        let id = document.getElementById('product_id').value;
        let product_name = document.getElementById('product_name').value;
        let productcat = document.getElementById('productcat').value;

        let req = new XMLHttpRequest()
        // console.log(id.trim().length);
        let url = '';
        if (id.trim().length > 0){
            url = 'http://127.0.0.1:8000/api/simpan-produk/'+id;
        }
        else{
            url = 'http://127.0.0.1:8000/api/simpan-produk';
        }
        // console.log(url);  
        
        req.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                senaraiProduk();
            }
        } 
        req.open('POST',url)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("name="+product_name+"&productcat="+productcat);

    } 
}
function kembali(){
    document.getElementById('borang').style.display= 'none';
    document.getElementById('senarai').style.display= 'block';
    document.getElementById('tambahbtn').style.display= 'block';
}

function tambah(){
    document.getElementById('borang').style.display= 'block';
    document.getElementById('senarai').style.display= 'none';
    document.getElementById('tambahbtn').style.display= 'none';
}
function kemaskini(id){
    document.getElementById('borang').style.display= 'block';
    document.getElementById('senarai').style.display= 'none';
    document.getElementById('tambahbtn').style.display= 'none';

    let req = new XMLHttpRequest()
    let url = 'http://127.0.0.1:8000/api/edit-produk/'+id;
    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let res = JSON.parse(this.responseText)
            document.getElementById('product_id').value = res.id;
            document.getElementById('product_name').value = res.name;
            document.getElementById('productcat').value = res.productcat;
        }
    }
    req.open('GET',url)
    req.send();
}

senaraiProduk()
function senaraiProduk(){
    document.getElementById('borang').style.display= 'none';
    document.getElementById('senarai').style.display= 'block';
    document.getElementById('tambahbtn').style.display= 'block';

    let req = new XMLHttpRequest()
    

    let search = document.getElementById('product_name_search').value;
    let searchcat = document.getElementById('product_cat_search').value;
    
    let url = '';
    let query = '';
    url = 'http://127.0.0.1:8000/api/senarai-produk' 
    if (search.trim().length > 0){
        query = '?product_name='+search
    }

    if (searchcat.trim().length > 0){
        if (query.trim().length > 0){
            query += '&productcat='+searchcat
        }else{
            query = '?productcat='+searchcat
        }
        
    }
    url += query;

    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            
            let res = JSON.parse(this.responseText);
            
            
            let cats = res.cat;
            for(const i in cats){
            
                $('#product_cat_search').append(`<option value="${cats[i].kod}">
                    ${cats[i].penerangan}
                </option>`);

                $('#productcat').append(`<option value="${cats[i].kod}">
                    ${cats[i].penerangan}
                </option>`);
            }
            
            

            
            let strData = '<table class="table table-striped">';
            
                strData += '<tr style="text-transform: capitalize;"><th>Bil</th>'; 
            
            let n = 1;
            
            strData += '<th>Produk</th><th>Kategori</th><th>Tindakan</th></tr>'; 
            for(const i of res.mprod){
                
                strData += `<tr style="text-transform: capitalize;"><td>${n++}.</td>`; 
                strData += '<td>'+ i.name +'</td>';
                strData += '<td>' + i.penerangan +'</td>';
                strData += `<td><button class="btn btn-info" onclick="kemaskini('${i.id}')">Kemaskini</button>`;
                strData += `<button class="btn btn-danger" onclick="hapus('${i.id}')" >Hapus</button></td><tr>`; 
            }
            strData += '</table>';
            document.getElementById('senarai').innerHTML = strData;
        }
    } 
    req.open('GET',url)
    req.send();
}

function hapus(id){
    if (confirm('Anda pasti?')){
        let req = new XMLHttpRequest()
        let url = 'http://127.0.0.1:8000/api/hapus-produk/'+id;
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                senaraiProduk();
            }
        } 
        req.open('GET',url)
        req.send();
    }
}
</script>
@endsection