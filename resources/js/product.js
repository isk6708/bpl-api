function simpan(){
    if (confirm('Anda pasti?')){
        let id = document.getElementById('product_id').value;
        let product_name = document.getElementById('product_name').value;
        let productcat = document.getElementById('productcat').value;

        let req = new XMLHttpRequest()
        // console.log(id.trim().length);
        let url = '';
        if (id.trim().length > 0){
            url = 'http://127.0.0.1:8000/api/simpan-produk/'+id;
        }
        else{
            url = 'http://127.0.0.1:8000/api/simpan-produk';
        }
        // console.log(url);  
        
        req.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                senaraiProduk();
            }
        } 
        req.open('POST',url)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("name="+product_name+"&productcat="+productcat);

    } 
}
function kembali(){
    document.getElementById('borang').style.display= 'none';
    document.getElementById('senarai').style.display= 'block';
    document.getElementById('tambahbtn').style.display= 'block';
}

function tambah(){
    document.getElementById('borang').style.display= 'block';
    document.getElementById('senarai').style.display= 'none';
    document.getElementById('tambahbtn').style.display= 'none';
}
function kemaskini(id){
    document.getElementById('borang').style.display= 'block';
    document.getElementById('senarai').style.display= 'none';
    document.getElementById('tambahbtn').style.display= 'none';

    let req = new XMLHttpRequest()
    let url = 'http://127.0.0.1:8000/api/edit-produk/'+id;
    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let res = JSON.parse(this.responseText)
            document.getElementById('product_id').value = res.id;
            document.getElementById('product_name').value = res.name;
            document.getElementById('productcat').value = res.productcat;
        }
    }
    req.open('GET',url)
    req.send();
}

senaraiProduk()
function senaraiProduk(){
    document.getElementById('borang').style.display= 'none';
    document.getElementById('senarai').style.display= 'block';
    document.getElementById('tambahbtn').style.display= 'block';

    let req = new XMLHttpRequest()
    let url = 'http://127.0.0.1:8000/api/senarai-produk'
    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            
            let res = JSON.parse(this.responseText);
            
            let strData = '<table class="table table-striped">';
            
                strData += '<tr style="text-transform: capitalize;"><th>Bil</th>'; 
            
            let n = 1;
            
            strData += '<th>Produk</th><th>Kategori</th><th>Tindakan</th></tr>'; 
            for(const i of res.mprod){
                
                strData += `<tr style="text-transform: capitalize;"><td>${n++}.</td>`; 
                strData += '<td>'+ i.name +'</td>';
                strData += '<td>' + i.productcat +'</td>';
                strData += `<td><button class="btn btn-info" onclick="kemaskini('${i.id}')">Kemaskini</button>`;
                strData += `<button class="btn btn-danger" onclick="hapus('${i.id}')" >Hapus</button></td><tr>`; 
            }
            strData += '</table>';
            document.getElementById('senarai').innerHTML = strData;
        }
    } 
    req.open('GET',url)
    req.send();
}

function hapus(id){
    if (confirm('Anda pasti?')){
        let req = new XMLHttpRequest()
        let url = 'http://127.0.0.1:8000/api/hapus-produk/'+id;
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                senaraiProduk();
            }
        } 
        req.open('GET',url)
        req.send();
    }
}