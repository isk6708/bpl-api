<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Ref;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $product_name = $req->product_name;
        $productcat = $req->productcat;

        $mprod = Product::select('products.id','products.name',
            'products.productcat','tbl_ref.penerangan')
            ->join('tbl_ref', 'products.productcat', '=', 'tbl_ref.kod')
        ->where(function($q) use ($product_name){
            if (!empty($product_name)){
                $q->where('name','like','%'.$product_name.'%');
            }
        })->where(function($q) use ($productcat){
            if (!empty($productcat)){
                $q->where('productcat',$productcat);
            }
        })
        ->get();
        $mcat = Ref::all();
        return response()->json(['mprod'=>$mprod,'cat'=>$mcat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Product::find($id)->delete();
    }

    public function simpan(Request $req,$id='')
    {
        if ($id == '')
            $mprod = new Product();
        else
            $mprod = Product::find($id);

        $mprod->name = $req->name;
        $mprod->productcat = $req->productcat;
        return $mprod->save();
    }
}
